package com.example.contacts;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int CODEFORSECONDACTIVITY = 1;
    private static final int CODEFORGROUPACTIVITY = 2;

    TextView name;
    TextView surname;
    TextView group;
    TextView phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        surname = findViewById(R.id.surname);
        group = findViewById(R.id.group);
        phone = findViewById(R.id.phone);
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivityForResult(intent, CODEFORSECONDACTIVITY);
    }

    public void editGroup(View view) {
        Intent intent = new Intent(this, Group.class);
        startActivityForResult(intent, CODEFORGROUPACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODEFORSECONDACTIVITY) {
            if (resultCode == RESULT_CANCELED) {
                name.setText(null);
                surname.setText(null);
                phone.setText(data.getStringExtra("key"));
                phone.setTextColor(Color.RED);
            } else if (resultCode == RESULT_OK) {
                name.setText(data.getStringExtra("name"));
                name.setTextColor(Color.parseColor("#8A2BE2"));
                surname.setText(data.getStringExtra("surname"));
                surname.setTextColor(Color.parseColor("#8A2BE2"));
                phone.setText(data.getStringExtra("phone"));
                phone.setTextColor(Color.parseColor("#8A2BE2"));
            }
        }

        if (requestCode == CODEFORGROUPACTIVITY && resultCode == RESULT_OK) {
            group.setText(String.format("Группа: %s", data.getStringExtra("group")));
            group.setTextColor(Color.parseColor("#8A2BE2"));
        }
    }
}
