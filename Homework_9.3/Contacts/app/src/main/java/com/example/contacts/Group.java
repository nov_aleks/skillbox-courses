package com.example.contacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Group extends AppCompatActivity {

    EditText editGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        editGroup = findViewById(R.id.editGroup);
    }

    public void onCloseEditGroup(View view) {
        Intent intentToFirstActivity = new Intent();
        intentToFirstActivity.putExtra("group", editGroup.getText().toString());
        setResult(RESULT_OK, intentToFirstActivity);
        this.finish();
    }
}
