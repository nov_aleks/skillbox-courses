import java.util.*;

public class ContactList {
    public static void main(String[] args) {
        TreeMap<String, String> contactList = new TreeMap<>();
        String[] keywords = {"list", "exit"};
        System.out.println("Contact list.");

        Scanner scanner = new Scanner(System.in);
        for(;;) {
            System.out.println("\nEnter something: ");
            String input = scanner.nextLine();

            if (input.isBlank()) {
                System.out.println("Your input is blank!");
                continue;
            }

            String number = input.strip().replaceAll("\\D", "");

            if (number.length() == 11) {
                String numFormatted = formatNumber(number);
                if (contactList.containsValue(numFormatted)) {
                    String name = getKey(contactList, numFormatted);
                    System.out.println(String.format("%s - %s", name, numFormatted));
                } else {
                    System.out.println("Please, enter your full name:");
                    for (;;) {
                        String name = scanner.nextLine();
                        if (Arrays.asList(keywords).contains(name)) {
                            System.out.println("You can not use system keywords!\nTry again!");
                        } else if (contactList.containsKey(name)) {
                            String num = contactList.get(name);
                            contactIsInList(name, num);
                        } else {
                            contactList.put(name, numFormatted);
                            contactIsAdded(name, numFormatted);
                            break;
                        }
                    }
                }
            } else if (input.equalsIgnoreCase("list")) {
                if (contactList.isEmpty()) {
                    System.out.println("There is no entries yet, add your first contact!");
                }
                for (Map.Entry entry: contactList.entrySet()) {
                    System.out.println(String.format("%s - %s", entry.getKey(), entry.getValue()));
                }
            } else if (input.equalsIgnoreCase("exit")) {
                break;
            } else {
                if (contactList.containsKey(input)) {
                    System.out.println(String.format("%s - %s", input, contactList.get(input)));
                } else {
                    System.out.println("Please, enter your number:");
                    for (;;) {
                        number = scanner.nextLine().strip().replaceAll("\\D", "");
                        if (number.length() == 11) {
                            String numFormatted = formatNumber(number);
                            if (contactList.containsValue(numFormatted)) {
                                String name = getKey(contactList, numFormatted);
                                contactIsInList(name, numFormatted);
                            } else {
                                contactList.put(input, numFormatted);
                                contactIsAdded(input, numFormatted);
                                break;
                            }
                        } else {
                            System.out.println("It is not a number, try again:");
                        }
                    }
                }
            }
        }
    }

    private static void contactIsInList(String name, String number)
    {
        System.out.println(String.format("This contact has been already added to the contact list.\n%s - %s\nPlease, try again!", name, number));
    }

    private static void contactIsAdded(String name, String number)
    {
        System.out.println(String.format("Your contact was added to the list:\n%s - %s", name, number));
    }

    private static String getKey(TreeMap<String, String> map, String value)
    {
        String result = null;
        for (Map.Entry entry: map.entrySet()) {
            if (entry.getValue().equals(value)) {
                result = entry.getKey().toString();
                break;
            }
        }

        return result;
    }

    private static String formatNumber(String input)
    {
        return input.replaceFirst("(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d{2})", "+$1 ($2) $3-$4-$5");
    }
}
