public class Truck {
    public static final int MAX_CAPACITY = 12;
    private String name = "Truck ";
    private Container[] containers;
    private static int count;

    public Truck()
    {
        count++;
        name += count;
    }

    public String getName()
    {
        return name;
    }

    public Container[] getContainers() {
        return containers;
    }

    public void setContainers(Container[] containers) {
        this.containers = containers;
    }
}
