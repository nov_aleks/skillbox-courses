package com.skillbox.android;

public class CardAccount extends PaymentAccount
{
    public CardAccount(double moneyAmount)
    {
        super(moneyAmount);
    }

    public void withdrawMoney(double amount)
    {
        if (this.isValid(amount)) {
            double transferFee = amount / 100;

            System.out.println(String.format("For withdrawing money from the account, 1%% = %.2f$ of the withdrawal amount will be charged.", transferFee));
            moneyAmount -= transferFee;
            moneyAmount -= amount;
        }
    }

    protected boolean isValid(double amount)
    {
        if (super.isValid(amount)) {
            double transferFee = amount / 100;

            if (amount + transferFee > moneyAmount) {
                System.out.println("Not enough money on your account taking into account the percentage of commission! Please enter a lower amount!");
                return false;
            }
        }

        return true;
    }
}
