import java.util.ArrayList;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        Company company = new Company();
        List<Employee> employees = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            employees.add(new Operator());
        }
        for (int i = 0; i < 30; i++) {
            employees.add(new Manager());
        }
        for (int i = 0; i < 10; i++) {
            employees.add(new TopManager());
        }
        company.hireAll(employees);

        System.out.println("\nTop salaries");
        for (Employee employee : company.getTopSalaryStaff(15)) {
            System.out.println(employee.getPosition().toString() + ": " + employee.getMonthSalary());
        }

        System.out.println("\nLowest salaries");
        for (Employee employee : company.getLowestSalaryStaff(60)) {
            System.out.println(employee.getPosition().toString() + ": " + employee.getMonthSalary());
        }

        company.firePart(Position.Manager, 50);
        company.firePart(Position.TopManager, 50);
        company.firePart(Position.Operator, 50);
        System.out.println("\nAfter firing");
        for (Employee employee : company.getEmployees()) {
            System.out.println(employee.getPosition().toString() + ": " + employee.getMonthSalary());
        }

        System.out.println("\nTop salaries");
        for (Employee employee : company.getTopSalaryStaff(15)) {
            System.out.println(employee.getPosition().toString() + ": " + employee.getMonthSalary());
        }
        System.out.println("\nLowest salaries");
        for (Employee employee : company.getLowestSalaryStaff(30)) {
            System.out.println(employee.getPosition().toString() + ": " + employee.getMonthSalary());
        }
    }
}
