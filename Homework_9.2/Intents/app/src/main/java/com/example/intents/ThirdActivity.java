package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity);

        Intent intent = getIntent();
        int number = intent.getIntExtra("number", 0);

        String str;
        if (number > 100) {
            str = "Your number is so big";
        } else if (number < 100) {
            str = "Your number is so small";
        } else {
            str = "My congratulations!";
        }

        TextView textView = findViewById(R.id.textView);
        textView.setText(str);
    }
}
