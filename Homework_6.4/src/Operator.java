public class Operator extends AbstractEmployee implements Employee
{
    public Position getPosition()
    {
        return Position.Operator;
    }

    public double getMonthSalary()
    {
        if (!isEmployed()) {
            return 0;
        }
        return getSalary();
    }
}
