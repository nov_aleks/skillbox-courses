package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    EditText numberInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        numberInput = findViewById(R.id.numberInput);
    }

    public void onOpenThirdActivity(View view) {
        Intent thirdActivityIntent = new Intent(this, ThirdActivity.class);
        int number = Integer.parseInt(numberInput.getText().toString());
        thirdActivityIntent.putExtra("number", number);
        startActivity(thirdActivityIntent);
    }
}
