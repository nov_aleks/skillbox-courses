import java.util.Scanner;

public class Loader
{
    public static void main(String[] args)
    {
        String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";
        String[] salaries = text.replaceAll("[^0-9\\s]", "").strip().split("\\s+");

        int sum = 0;
        for (String salary: salaries) {
            sum += Integer.parseInt(salary);
        }
        System.out.println(salaries.length);

        System.out.println(text);
        System.out.println(String.format("Общий заработок: %d рублей", sum));
        System.out.println();

        String[] fioMembers = {"Фамилия", "Имя", "Отчество"};

        String fio = "";
        do {
            System.out.println("Введите ФИО:");
            Scanner scanner = new Scanner(System.in);
            fio = scanner.nextLine().strip();
            if (fio.equals("")) {
                System.out.println("Нет данных, попробуйте ещё раз.");
            }
        } while (fio.equals(""));

        String[] parts = fio.split("\\s+");

        for (int p = 0; p < fioMembers.length; p++) {
            if (p < parts.length) {
                System.out.println(String.format("%s: %s", fioMembers[p], parts[p]));
            }
        }

        System.out.println();

        String number = "";
        do {
            System.out.println("Please, put in your phone number:");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine().strip().replaceAll("[\\D]", "");
            if (input.length() == 11) {
                number = input;
            } else {
                System.out.println("The number should be 11 digits.");
            }
        } while (number.equals(""));

        System.out.println(number.replaceFirst("(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d{2})", "+$1 $2 $3-$4-$5"));
    }
}