import java.util.*;

public class Company
{
    public static final double INCOME_GOAL = 10000000;
    private List<Employee> employees;

    public Company()
    {
        employees = new ArrayList<>();
    }

    public void hire(Employee employee)
    {
        employee.setEmployeeCompany(this);
        employees.add(employee);
    }

    public void hireAll(List<Employee> employees)
    {
        employees.forEach(this::hire);
    }

    public void fire(Employee employee)
    {
        employees.remove(employee);
    }

    public void firePart(Position position, int percent)
    {
        List<Employee> employeesByPosition = new ArrayList<>();
        employees.forEach(employee -> {
            if (employee.getPosition().toString().equals(position.toString())) {
                employeesByPosition.add(employee);
            }
        });

        List<Employee> employeesToFire = new ArrayList<>();
        int count = getCount(employeesByPosition, percent);

        if (count > 0) {
            employeesToFire = employeesByPosition.subList(0, count);
        }
        employees.removeAll(employeesToFire);
    }

    public List<Employee> getEmployees()
    {
        return employees;
    }

    public void setEmployees(List<Employee> employees)
    {
        this.employees = employees;
    }

    public double getIncome()
    {
        return employees.stream()
                .filter(employee -> employee instanceof Manager)
                .map(Manager.class::cast)
                .map(Manager::getSalesAmount)
                .reduce(0.0, Double::sum);
    }

    public List<Employee> getTopSalaryStaff(int count)
    {
        return getSalaryStaff(count, getComparator().reversed());
    }

    public List<Employee> getLowestSalaryStaff(int count)
    {
        return getSalaryStaff(count, getComparator());
    }

    private List<Employee> getSalaryStaff(int count, Comparator<Employee> comparator)
    {
        List<Employee> salaryStaff = new ArrayList<>();
        employees.sort(comparator);
        count = Math.min(count, employees.size());
        for (int i = 0; i < count; i++) {
            salaryStaff.add(employees.get(i));
        }
        return salaryStaff;
    }

    private Comparator<Employee> getComparator()
    {
        return (Employee o1, Employee o2) -> {
            if (o1.getMonthSalary() > o2.getMonthSalary()) {
                return 1;
            }
            if (o1.getMonthSalary() < o2.getMonthSalary()) {
                return -1;
            }
            return 0;
        };
    }

    private  int getCount(List elements, int percent)
    {
        percent = Math.min(percent, 100);
        return elements.size() * percent / 100;
    }
}
