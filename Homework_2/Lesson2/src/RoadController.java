import core.*;
import core.Camera;

import java.util.Scanner;

// Программа анализирует возможность и стоимость проезда автомобилей на основе их характеристик.
public class RoadController
{
    // переменная типа double
    private static double passengerCarMaxWeight = 3500.0; // kg

    // переменная типа int
    private static int passengerCarMaxHeight = 2000; // mm

    // переменная типа int
    private static int controllerMaxHeight = 4000; // mm

    // переменная типа int
    private static int passengerCarPrice = 100; // RUB

    // переменная типа int
    private static int cargoCarPrice = 250; // RUB

    // переменная типа int
    private static int vehicleAdditionalPrice = 200; // RUB

    // в параметрах массив переменных типа String
    public static void main(String[] args)
    {
        System.out.println("Сколько автомобилей сгенерировать?");

        // переменная класса Scanner
        Scanner scanner = new Scanner(System.in);

        // переменная типа int
        int carsCount = scanner.nextInt();

        // i переменная типа int
        for(int i = 0; i < carsCount; i++)
        {
            // переменная класса Car
            Car car = Camera.getNextCar();
            System.out.println(car);

            //Пропускаем автомобили спецтранспорта бесплатно
            if (car.isSpecial) {
                openWay();
                continue;
            }

            // переменная типа int
            //Проверяем высоту и массу автомобиля, вычисляем стоимость проезда
            int price = calculatePrice(car);
            if(price == -1) {
                continue;
            }

            System.out.println("Общая сумма к оплате: " + price + " руб.");
        }
    }

    /**
     * Расчёт стоимости проезда исходя из массы и высоты
     */
    private static int calculatePrice(Car car)
    {
        // переменная типа int
        int carHeight = car.height;

        // переменная типа int
        int price = 0;
        if (carHeight > controllerMaxHeight)
        {
            blockWay("высота вашего ТС превышает высоту пропускного пункта!");
            return -1;
        }
        else if (carHeight > passengerCarMaxHeight)
        {
            // переменная типа double
            double weight = car.weight;
            //Грузовой автомобиль
            if (weight > passengerCarMaxWeight)
            {
                price = passengerCarPrice;
                if (car.hasVehicle) {
                    price = price + vehicleAdditionalPrice;
                }
            }
            //Легковой автомобиль
            else {
                price = cargoCarPrice;
            }
        }
        else {
            price = passengerCarPrice;
        }
        return price;
    }

    /**
     * Открытие шлагбаума
     */
    private static void openWay()
    {
        System.out.println("Шлагбаум открывается... Счастливого пути!");
    }

    /**
     * Сообщение о невозможности проезда
     *
     * в параметрах переменная типа String
     */
    private static void blockWay(String reason)
    {
        System.out.println("Проезд невозможен: " + reason);
    }
}