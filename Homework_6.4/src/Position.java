public enum Position
{
    TopManager(10000),
    Manager(1500),
    Operator(500);

    private double salary;

    Position (double salary)
    {
        this.salary = salary;
    }

    public double getSalary()
    {
        return salary;
    }
}
