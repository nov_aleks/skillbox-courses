package com.skillbox.android.bank.client;

// юридическое лицо
public class Entity extends Client
{
    public Entity()
    {
        setPercentMap();
    }

    public void withdrawMoney(double amount)
    {
        if (amount > 0) {
            try {
                double transferFee = amount / 100 * getPercent(amount);
                System.out.println(String.format("For withdrawing money from the account, 1%% = %.2f$ of the withdrawal amount will be charged.", transferFee));
                super.withdrawMoney(amount + transferFee);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Amount have to be higher than 0.");
        }
    }

    private void setPercentMap()
    {
        getPercentMap().put(0, 1.0);
    }
}
