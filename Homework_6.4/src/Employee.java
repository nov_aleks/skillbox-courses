public interface Employee
{
    public double getMonthSalary();

    public void setEmployeeCompany(Company company);

    public Position getPosition();
}
