public class Main
{
    public static void main(String[] args)
    {
        Container container = new Container();
        container.count += 7843;

        System.out.println(sumDigits(45));
    }

    public static Integer sumDigits(Integer number)
    {
        String numStr = Integer.toString(number);
        int sum = 0;
        for (int i = 0; i < numStr.length(); i++) {
            sum += Character.getNumericValue(numStr.charAt(i));
        }

        return sum;
    }
}
